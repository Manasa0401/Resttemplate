package com.img.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.img.user.entity.User;
import com.img.user.service.UserService;

@RestController

public class UserController {
	
	@Autowired
	UserService userService;

	//create user
	@PostMapping("/createUser")
	public User addUser(@RequestBody User user){
		 return userService.saveUser(user);
	}
	
	@GetMapping("userByEmail/{email}")
	public User findUserbyEmail(@PathVariable String email) {
		return userService.getUserByEmail(email);
	}
	
	@GetMapping("/getusers")
	public List<User> findAllUsers(){
		return userService.getUsers();
	}
	 
	
}


