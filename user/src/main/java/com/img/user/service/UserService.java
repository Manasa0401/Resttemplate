package com.img.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.img.user.entity.User;
import com.img.user.repository.UserRepository;



@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	public User saveUser(User users){
	  return userRepository.save(users);
	}
	
	public List<User> getUsers(){
		return userRepository.findAll();
	}
	
  public User getUserByEmail(String email ) {
	  return userRepository.findByEmail(email);
	
}
  
	
}




