package com.img.employee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.img.employee.entity.Employee;







@RestController
public class EmployeeController {

	
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	@Autowired
	private RestTemplate restTemplate;
	
	static final String USER_URL = "http://localhost:8081/";
	
	@GetMapping("/find/{email}")
	public String fetchEmployee(@PathVariable String email) {
		Employee employee = restTemplate.exchange(USER_URL+"userByEmail/"+email, HttpMethod.GET,null,Employee.class).getBody();
		System.out.println("Student from Student Report Project :"+employee);
		return restTemplate.getForObject(USER_URL+"userByEmail/"+email, String.class);
	}
	
	@PostMapping("/addUser")
	public String addEmployee(@RequestBody Employee employee) {
		return restTemplate.postForObject(USER_URL+"createUser", employee, String.class);
	}
	
}






